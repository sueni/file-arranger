use std::error::Error;
use std::ffi::{OsStr, OsString};
use std::path::{Path, PathBuf};
use std::time::{SystemTime, UNIX_EPOCH};
use std::{fs, io};

mod cli;

#[derive(Debug)]
struct Batch {
    pub most_recent: SystemTime,
    pub paths: Vec<PathBuf>,
}

impl Batch {
    pub fn new() -> Self {
        Self {
            most_recent: UNIX_EPOCH,
            paths: Vec::new(),
        }
    }

    pub fn push(&mut self, item: PathBuf) {
        if let Ok(mtime) = mtime(&item) {
            if self.most_recent < mtime {
                self.most_recent = mtime;
            }
            self.paths.push(item);
        }
    }

    pub fn is_filled(&self) -> bool {
        !self.paths.is_empty()
    }

    pub fn sort(&mut self) {
        self.paths
            .sort_unstable_by(|a, b| alphanumeric_sort::compare_path(&a, &b));
    }

    pub fn reverse(&mut self) {
        self.paths.reverse();
    }
}

fn mtime(item: &Path) -> Result<SystemTime, Box<dyn Error>> {
    Ok(item.metadata()?.modified()?)
}

struct Collector<'a> {
    exts: &'a [&'a OsStr],
    content: Vec<Batch>,
}

impl<'a> Collector<'a> {
    pub fn new(exts: &'a [&OsStr]) -> Self {
        Self {
            exts,
            content: Vec::new(),
        }
    }

    pub fn collect(&mut self, path: &dyn AsRef<Path>) {
        if let Ok(dir) = fs::read_dir(path) {
            for entry in dir.flatten() {
                let entry = entry.path();
                if entry.is_dir() {
                    self.collect_nested(&entry);
                } else if let Some(ext) = entry.extension() {
                    if self.exts.contains(&ext) {
                        let mut parts = Batch::new();
                        parts.push(entry);
                        self.content.push(parts)
                    }
                }
            }
        }
    }

    fn collect_nested(&mut self, path: &dyn AsRef<Path>) {
        if let Ok(dir) = fs::read_dir(path) {
            let mut parts = Batch::new();
            for entry in dir.flatten() {
                let entry = entry.path();
                if entry.is_dir() {
                    self.collect(&entry);
                } else if let Some(ext) = entry.extension() {
                    if self.exts.contains(&ext) {
                        parts.push(entry);
                    }
                }
            }
            if parts.is_filled() {
                self.content.push(parts)
            }
        }
    }

    pub fn sort(&mut self, reverse: bool) {
        self.content.iter_mut().for_each(|batch| batch.sort());
        self.content
            .sort_unstable_by(|a, b| b.most_recent.cmp(&a.most_recent));
        if reverse {
            self.content.reverse();
            if self.content.len() == 1 {
                self.content[0].reverse();
            }
        }
    }

    pub fn display(&self) {
        use std::io::Write;
        use std::os::unix::ffi::OsStrExt;
        for parts in &self.content {
            for s in &parts.paths {
                io::stdout().write_all(s.as_os_str().as_bytes()).unwrap();
                io::stdout().write_all(b"\n").unwrap();
            }
        }
    }
}

fn main() {
    match cli::parse_args() {
        Err(err) => {
            eprintln!("{}\n{}", err, cli::HELP);
            std::process::exit(1);
        }
        Ok(args) => {
            let exts: Vec<&OsStr> = args.exts.iter().map(OsString::as_os_str).collect();
            let mut collector = Collector::new(exts.as_slice());
            collector.collect(&args.dir);
            collector.sort(args.reverse);
            collector.display();
        }
    }
}
