use std::ffi::OsString;
use std::path::PathBuf;

pub const HELP: &str = "
USAGE:
  file-arranger [FLAGS] [OPTIONS] -- <ARGS>

FLAGS:
  -h               Show help
      --reverse    Reverse files order

OPTIONS:
      --dir <DIR>  Working directory (default: '.')

REQUIRED ARGS:
  E1 E2 ... EN     Files extensions
";

pub struct Args {
    pub reverse: bool,
    pub dir: PathBuf,
    pub exts: Vec<OsString>,
}

pub fn parse_args() -> Result<Args, lexopt::Error> {
    use lexopt::prelude::*;

    let mut reverse = false;
    let mut dir = PathBuf::from(".");
    let mut exts: Vec<OsString> = Vec::new();
    let mut parser = lexopt::Parser::from_env();

    while let Some(arg) = parser.next()? {
        match arg {
            Long("reverse") => reverse = true,
            Long("dir") => dir = parser.value()?.into(),
            Value(ext) => {
                exts.push(ext.to_ascii_uppercase());
                exts.push(ext);
            }
            Short('h') => {
                println!("{}", HELP);
                std::process::exit(0);
            }
            _ => return Err(arg.unexpected()),
        }
    }

    if exts.is_empty() {
        return Err("ERROR: need extensions to collect files".into());
    }

    Ok(Args { reverse, dir, exts })
}
